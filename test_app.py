from requests import get
import unittest

class TestApp(unittest.TestCase):
    def test_add(self):
        result = get("http://13.209.15.210:8176/add", params={"a":1,"b":2})
        real_result = result.text
        self.assertEqual(real_result, "3")
    def test_sub(self):
        result = get("http://13.209.15.210:8176/sub", params={"a":1,"b":2})
        real_result = result.text
        self.assertEqual(real_result, "-1")

if __name__ == "__main__":
    unittest.main()

