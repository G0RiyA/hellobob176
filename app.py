from flask import Flask, request

app = Flask(__name__)

@app.route('/')
def index():
    return "Hello Bob from user176"

@app.route('/add')
def add():
    try:
        a = int(request.args.get('a', 0))
        b = int(request.args.get('b', 0))
        return f'{a + b}'
    except:
        return 'Invalid request'

@app.route('/sub')
def sub():
    try:
        a = int(request.args.get('a', 0))
        b = int(request.args.get('b', 0))
        return f'{a - b}'
    except:
        return 'Invalid request'

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8176)
